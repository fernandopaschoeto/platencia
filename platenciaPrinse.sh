#!/usr/bin/env bash

#-----------HEADER-------------------------------------------------------------|
# AUTOR             : FERNANDO PASCHOETO
# HOMEPAGE          : https://gitlab.com/fernandopaschoeto
# DATA-DE-CRIAÇÃO   : 2020-08-25
# PROGRAMA          : PLATENCIA
# VERSÃO            : 1.4
# LICENÇA           : GPL3
# PEQUENA-DESCRIÇÃO : Programa coleta latencia de conexão com hosts e se há perdas
#                       de pacote e envia os dados para o Telegram. 
# CHANGELOG : 
#
#   Versão-1.0 | FERNANDO PASCHOETO - AGOSTO DE 2020
#               Programa realiza ping de uma lista de endereços e retorna em 
#               um arquivo de log o nome do host e resultado do comando ping. 
#
#   Versão-1.1 | FERNANDO PASCHOETO - Agosto de 2020
#               Criado a função lapidar que remove do texto as informações 
#                desnecessárias. 
#
#   Versão-1.2 | FERNANDO PASCHOETO - Agosto de 2020
#               Adicionado verificação de perdas de pacote
#               Melhoria ao lapdar arquivos diminuindo as linhas de código.
#
#   Versão-1.3 | FERNANDO PASCHOETO - Setembro de 2020
#               Reduzido número de pings e apontando para 2 arquivos temporários.
#               Alterada a forma como o programa coleta os dados. 
#
#   Versão-1.4 | FERNANDO PASCHOETO - Setembro de 2020
#               Criado função para integração com o Telegram.
#               Realizada alteração na saída do código para ficar legível 
#                   em dispositivos móveis. 
#
#------------------------------------------------------------------------------|


#--------------------------------- VARIÁVEIS ---------------------------------->
LOG=platencia.tmp
perdaspct=perdaspct.tmp
recorte=recorte.tmp
mensagem="0"

# Hosts e IP precisam estar com a mesma ordem para não alterarem a saída. 
hosts=(
"Google" 
"Facebook" 
"FreeDNS"
"Youtube"

)

ip=(
"8.8.8.8" 
"facebook.com" 
"1.1.1.1"
"youtube.com"

)


#------------------------------- FIM-VARIÁVEIS --------------------------------<

# Realiza o processo de ping nos hosts. 
function pingando(){
    i=0
    h=0
    pacotes=0
    for i in "${ip[@]}"; do
        echo -e "\t\t\n\n\t________________________________\n\n<===> ${hosts[$h]} <===>\n\n"
        
        ping -c 5 "$i" | tee "$recorte" "$perdaspct"
        pacotes=$(grep packets "$perdaspct" | cut -d ' ' -f4)
            # Testa se há perdas de pacote para os hosts
            if [[ "$pacotes" -eq "5" ]]; then
                echo "Sem perdas para ${hosts[h]}!! <===>"
                        
            elif [[ "$pacotes" -eq "0" ]]; then
                echo "Sem conexao com ${hosts[h]}!! <===> "

            else
                echo "${hosts[h]} com perdas de pacote!! <===>"
            fi
       
    let h++
            
    done
    
}


# Retira informação indesejada do arquivo. 
# Essa função ainda deve ser melhorada, ainda dependendo do direcionamento para o telegram
function lapidar(){

        sed -i 's/rtt//g' "$LOG"
        sed -i 's/\/mdev//g' "$LOG" 
        sed -ri 's/\/[0-9]{1,3}.[0-9]{1,3} ms$/ ms/' "$LOG" 
        mensagem=$(grep -E "________________________|<===>|min/avg" "$LOG")
      
}

function telegram(){
    # Telegram Variáveis
    token="SEU TOKEN TELEGRAM"
    ID="ID DO GRUPO OU CHAT TELEGRAM"
    URL="https://api.telegram.org/bot$token/sendMessage"
           
           # Envia saída para Grupo do Telegram. 
            curl -s -X POST $URL -d chat_id=$ID -d text="$mensagem "

}

#------------------------------- CHAMADA DE FUNÇÕES --------------------------------<

pingando >${LOG}
lapidar
telegram
