# P-Latência


###### Automação para testes de latência

P-Latência é um script para automatizar testes de latência para vários hosts retornando para um bot do telegram o tempo mínimo, médio, máximo e apresentando se há ou não perdas de pacote.

O programa foi desenvolvido em shellscript e executa requisições ICMP para uma lista de hosts pré definida enviando os resultados desejados por um bot para um grupo do Telegram.  

## Preparando o ambiente

O script P-Latência foi desenvolvido em bash e roda em qualquer sistema linux, pode ser utilizado em conjunto com o agendador de tarefas Cron para ser executado automaticamente em dias e horários pré definidos. Você pode ver o tutorial de como configurar o crontab para agendar execução de tarefas em sistemas Linux neste "link".

O script por trabalhar com arquivos de texto puro, praticamente não consome recursos de sistema apenas cerca de 20 Megas de armazenamento dependendo da quantidade de hosts configurados. 



## O Script

Para a criação do script tomei como opção a criação de funções dividindo cada etapa do funcionamento e visando facilitar a execução e manutenção do código que embora seja simples, utiliza alguns recursos builtin do sistema. 



### Variáveis globais

Fora das funções foram criadas as variáveis responsáveis pela coleta e tratamento das informações, são elas: 

- **LOG**: arquivo de log para armazenar toda a execução do programa. 
- **perdaspct**: arquivo que armazena as perdas de pacote. 
- **recorte**: arquivo que irá armazenar as informações removendo o que não é necessário. 
- **mensagem**: informação já tratada para envio no telegram.  



```shell
LOG=platencia.tmp
perdaspct=perdaspct.tmp
recorte=recorte.tmp
mensagem="0"

```

### Configurando os hosts

Os hosts que serão testados precisam são adcionados em duas listas, sendo necessário estar em ordem e separados por aspas duplas para que o resultado seja exibido perfeitamente. Para demonstração farei os testes para os endereços:   GoogleDNS, Facebook, FreeDNS , Youtube e Endereço-Falso mas a lista pode conter quantos hosts forem necessários. 

```shell
hosts=(
"Google" 
"Facebook" 
"FreeDNS"
"Youtube"
"Endereço-Falso"
)

```

```shell
ip=(
"8.8.8.8" 
"facebook.com" 
"1.1.1.1"
"youtube.com"
"falso.com.br"
)

```



## As Funções

A execução do programa foi dividida em 3 funções, a primeira responsável por realizar os testes de ping e salvar os resultados nos arquivos, a segunda faz o tratamento dos dados e a organização nos arquivos e a terceira é responsável pelo envio das informações já tratadas pelo o bot do telegram. 



### função 1 - Pingando

```shell
function pingando(){
    i=0
    h=0
    pacotes=0
    for i in "${ip[@]}"; do
        echo -e "\t\t\n\n\t________________________________\n\n<===> ${hosts[$h]} <===>\n\n"
        
        ping -c 5 "$i" | tee "$recorte" "$perdaspct"
        pacotes=$(grep packets "$perdaspct" | cut -d ' ' -f4)
            # Testa se há perdas de pacote para os hosts
            if [[ "$pacotes" -eq "5" ]]; then
                echo "Sem perdas para ${hosts[h]}!! <===>"
                        
            elif [[ "$pacotes" -eq "0" ]]; then
                echo "Sem conexao com ${hosts[h]}!! <===> "

            else
                echo "${hosts[h]} com perdas de pacote!! <===>"
            fi
       
    let h++
            
    done
    
}


```

### Função 2 - Lapidar

```shell
function lapidar(){

        sed -i 's/rtt//g' "$LOG"
        sed -i 's/\/mdev//g' "$LOG" 
        sed -ri 's/\/[0-9]{1,3}.[0-9]{1,3} ms$/ ms/' "$LOG" 
        mensagem=$(grep -E "________________________|<===>|min/avg" "$LOG")
      
}

```



### Função 3 - Telegram

```shell
function telegram(){
    # Telegram Variáveis
    token="SEU TOKEN TELEGRAM"
    ID="ID DO GRUPO OU CHAT TELEGRAM"
    URL="https://api.telegram.org/bot$token/sendMessage"
           
           # Envia saída para Grupo do Telegram. 
            curl -s -X POST $URL -d chat_id=$ID -d text="$mensagem "

}

```

Neste "link" temos um tutorial completo de como criar e configurar um bot no Telegram para rodar o P-Latencia e várias outras aplicações. 



## Executando 

Para a execução das funções apenas apliquei a chamada de cada uma em ordem no script Shell e direcionei para o arquivo LOG. 



```shell
pingando >${LOG}
lapidar
telegram

```



## Conclusão

P-Latencia é um script funcional que executa os testes para vários hosts e retorna as informações automatizando a análise de falhas para antecipandar possíveis problemas em uma rede. Simples de ser configurado e implantado tem o intuito de automatizar tarefas repetitivas dos analistas de rede. 
